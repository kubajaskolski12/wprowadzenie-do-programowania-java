Zadania Pętle

Zadanie 1
Napisz program, który wypisuje wszystkie liczby od 5 do 10.

Zadanie 2
Napisz program, który na ekran wypisuje 5 razy napis Hello.

Zadanie 3
Napisz program, który wypisuje liczby od 5 do 50, które są nieparzyste.

Zadanie 4
Napisz program, który wypisuje liczby od 50 do 10 (w tej kolejności).

Zadanie 5
Napisz program, który odczytuje n i sumuje liczby od 1 do n.

Zadanie 6
Napisz prostą grę - zadaniem użytkownika będzie zgadnięcie liczby, którą
zainicjujemy w programie (przykładowa liczba 600). W przypadku, gdy liczba
będzie za duża lub za mała, użytkownik otrzyma odpowiednią podpowiedź.
Gramy tak długo dopóki użytkownik zgadnie liczbę.

Zadanie 7
Napisz program, który losuje 6 liczb z Dużego Lotka.

Zadanie 8
Napisz program, który prosi o podanie poprawnego hasła( hasło to Polska), tak
długo jak użytkownik nie odgadnie hasła wyświetlany jest komunikat podaj
poprawne hasło.

Zadanie 9
Napisz program, który odczytuje n i oblicza n!.

Zadanie 10
Napisz program który wygeneruje za pomocą (wielkość wieżyczki podaje
użytkownik)
a )wieżyczkę
*
**
***
****
b) choinkę
   *
  ***
 *****
*******

Zadanie 11
Odczytaj wyraz i wypisz wszystkie cyfry występujące w wyrazie.

Zadanie 12
Napisz program, który odczytuje wyraz i sprawdza czy wyraz jest palindromem.

Zadanie 13
Napisz program drukujący na ekranie 19 gwiazdek:
*******************

Zadanie 14
Napisz program, który odczytuje liczbę i sprawdza czy liczba jest pierwsza czy
złożona.

Zadanie 15
Napisz program, który odczytuje wyraz i wypisuje go w odwrotnej kolejności.

Zadanie 16
Stwórz program, który odczytuje dany napis i wypisuje ile razy w danym napisie
występują małe litery. Przykładowo dla napisu: aAaaBssk wynikiem powinno być
6 (małe a występuje 3 razy, s występuje 2 razy, k występuje 1 raz).
Zadanie 17
Napisz program, który wypisuje co drugą literę imienia.

Zadanie 18
Napisz program, który oblicza największy wspólny dzielnik dwóch liczb.