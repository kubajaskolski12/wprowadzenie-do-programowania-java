Zadania Tablice

Zadanie 1
Napisz metodę, która dla danej tablicy liczb całkowitych zwraca pierwszy element
tablicy.
first([1,2,3,4]) = 1
first(4,9,12,1]) = 4

Zadanie 2
Napisz metodę, która dla danej tablicy liczb całkowitych zwraca ostatni element
tablicy.

Zadanie 3
Napisz metodę, która jako argument przyjmuje zawsze tablice składającą się z
dwóch elementów liczb całkowitych. Metoda ma zwróci sumę elementów tablicy.
sum([1,2]) = 3
sum([4,8]) = 12

Zadanie 4
Napisz metodę, która dla danej tablicy zwraca sumę pierwszego i ostatniego
elementu tablicy.
sumArray([1, 2, 3]) = 1+3 = 4

Zadanie 5
Napisz metodę, która zwraca element środkowy tablicy.
middleElement(([1, 2, 3]) = 2
middleElement(([1, 5, 3, 4]) = 5

Zadanie 6
Zakładamy, że mamy 2 tablice 2 elementowe. Nasza metoda powinna zwrócić
nową tablicę, która jest połączona z dwóch tablic.
plusTwoArrays([-4, 4], [8, 2]) → [-4, 4, 8, 2]
plusTwoArrays([9, 1], [3, 10]) → [9, 1, 3, 10]

Zadanie 7
Napisz metodę, która zwraca sumę elementów tablicy całkowitej.
sum([1,2,3]) = 6

Zadanie 8
Napisz metodę, która zwraca największy element tablicy całkowitej.
max([2,3,5,1,20,25]) = 25

Zadanie 9
Napisz metodę, która odwraca daną tablicę liczb całkowitych.
swap([1,2,3]) = [3,2,1]

Zadanie 10
Co z tym kodem jest nie tak?
int[] array = new int[5];
for (int i=0;i <=5;i++){
array[i] =12;
}

Zadanie 11
Napisz metodę, która zwraca posortowaną tablicę liczb.
mySort([4,1,9,15]) = [1,4,9,15]

Zadanie 12
Napisz metodę, która zwraca tablice elementów środkowych.
makeMiddle([1,2,3,4]) = [2, 3]
Zakładamy, że długość tablicy jest zawsze podzielna przez 2.

Zadanie 13
Napisz metodę, która jako argument przyjmuje tablicę Stringów. Jako wynik ma
zwracać tablice wszystkich słów, które zawierają słowa 5-literowe.

Zadanie 14
Napisz metodę, która jako argument przyjmuje tablicę Stringów. Jako wynik ma
zwracać tablice tej samej długości, w której wyrazy są napisane małymi literami(
duże litery zamieniamy na małe).

Zadanie 15
Napisz metodę, która jako argument przyjmuje tablice Stringów. Jako wynik
metoda ma zwracać liczbę całkowitą, która oznacza najdłuższy wyraz w tablicy.

Zadanie 16
Napisz metodę, która jako argument przyjmuje tablicę liczb. Jako wynik metoda
powinna zwrócić tablicę wszystkich liczb, które mają dokładnie dwa dzielniki.

Zadanie 17
Napisz metodę, która jako argument zawiera tablicę elementów typu boolean.
Metoda ma zwrócić nową tablicę, która zawiera tyle elementów true ile znajduje
się w bazowej tablicy.
newArray([false,true,true,false,false]) = [true,true]
newArray([true,true,true,true,false,false, true]) = [true,true,true,true,true]

Zadanie 18
Napisz metodę, która wyznaczy liczbę wystąpień liczby k w danym ciągu.
numberOfElements([1,2,3,3,3,4,3],3) = 4 ( 3 występuje 4 razy w danym ciągu).
numberOfElements([1,2,3,3,3,4,3],8) = 0 ( 8 nie występuje ani razu w danym ciągu).