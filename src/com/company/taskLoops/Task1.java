package com.company.taskLoops;

public class Task1 {

    public static void main(String[] args) {

//        Zadanie 1
//        Napisz program, który wypisuje wszystkie liczby od 5 do 10.

        for (int i = 5; i < 11; i++) {
            System.out.println(i);
        }
    }

}
