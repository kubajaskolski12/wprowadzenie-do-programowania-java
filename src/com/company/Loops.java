package com.company;

public class Loops {

    /*
    Rodzaje pętli:
    - for i
    - while
    - do while
    - for each
    instrukcje:
   -break
   -continue
   */

    public static void main(String[] args) {

        String[] strings = {"Ala", "Ania", "Tomek", "Marek", "Adam"};

        for (String name : strings) {
            if(name.startsWith("A"))
                continue;
            System.out.println(name);
        }

    }
}
