package com.company;

public class DataTypes {

    /*
     typy proste

     byte - 1 bajt - zakres od -128 do 127 0
     short - 2 bajty - zakres od -32 768 do 32 767 0
     int - 4 bajty - zakres od -2 147 483 648 do 2 147 483 647 0
     long - 8 bajtów - zakres od -2^63 do (2^63)-1 (posiadają przyrostek L, lub l) 0

     float - 4 bajty - max ok 6-7 liczb po przecinku (posiadają przyrostek F, lub f) 0.0
     double - 8 bajtów - max ok 15 cyfr po przecinku (posiadają przyrostek D, lub d) 0.0
     Część całkowitą od ułamkowej oddzielamy kropką, a nie przecinkiem!

     char - pojedynczy znak do zapisu używamy '' np. 'a'

     boolean - przechowuje wartość typu prawda, fałsz (true, false) false

     typy złożone

     String - przechowuje on ciąg znaków do zapisu używamy "" np "Ala ma kota" null
     BigDeciaml, BigInteger  - typy liczbowe do posługiwania się bardzo dużymi liczbami lub z bardzo dużą
                               dokładnością null
     Boolean - okrreśla prawdę bądź fałsz dodatkowo przyjmuje wartość null

     Object - wszystkie typy obiektowe null
     */

    //psvm

    public static void main(String[] args) {

        int age = 10;
        String name = "Marcin";

        //sout
        System.out.println(name);
    }
}
