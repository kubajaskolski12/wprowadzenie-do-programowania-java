package com.company.arrys;

public class Array1 {


    public static void main(String[] args) {
        int[] numbers = new int[6];
        numbers[0] = 1;
        numbers[1] = 13;
        numbers[2] = 123;

        String[] names = new String[5];
        names[0] = "Tomek";
        names[1] = "Ala";
        names[2] = "Marek";
        names[3] = "Adam";
        names[4] = "Filip";

        System.out.println();
    }
}
