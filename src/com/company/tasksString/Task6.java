package com.company.tasksString;

import java.util.Scanner;

public class Task6 {

    public static void main(String[] args) {
//        Zadanie 4.
//        Napisz program, który odczytuje imię i zamienia je na imię rozpoczynające się od dużej litery:
//
//        Dane:
//        piotr
//        Wynik:
//        Piotr

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj proszę imie");
        String name = sc.next();
        String firstLetter = name.substring(0, 1);
        String restOfName = name.substring(1);
        System.out.println(firstLetter.toUpperCase() + restOfName);



    }
}
