package com.company.tasksString;

import java.util.Scanner;

public class Task2 {

    //        Zadanie 2
//            Sprawdź czy dana liczba jest podzielna przez 3

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj proszę liczbę");
        int number = sc.nextInt();
        if(number % 3 == 0){
            System.out.println("Liczba jest podzielna przez 3");
        }
        else{
            System.out.println("Liczba nie jest podzielna przez 3");
        }


    }

}

