package com.company.tasksString;

import java.util.Scanner;

public class Task4 {

    public static void main(String[] args) {
//        Zadanie 2.
//        Napisz program, który odczytuje imię i wypisuje długość
//        wprowadzonego imienia:
//
//        Dane:
//        Piotr
//        Na ekran:
//        5
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj proszę swoje imie:");
        String name = sc.next();
        System.out.println(name.length());
    }
}
