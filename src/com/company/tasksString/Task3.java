package com.company.tasksString;

import java.util.Scanner;

public class Task3 {

    public static void main(String[] args) {

//        Zadanie 1.
//        Napisz program, który odczytuje 3 wyrazy i wypisuje je w odwrotnej kolejności.

        /*
        Ala ma kota
        kota ma Ala
         */

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj wyraz 1");
        String word1 = sc.next();
        System.out.println("Podaj wyraz 2");
        String word2 = sc.next();
        System.out.println("Podaj wyraz 3");
        String word3 = sc.next();

        System.out.println("Podane wyrazy w odworotenej kolejności to: " + word3 + " " + word2 + " " + word1);


    }
}
